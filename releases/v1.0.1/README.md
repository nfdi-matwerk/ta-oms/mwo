# The MatWerk Ontology v1.0.1

## Release notes

This release includes the following changes:

- adding versioning information to the OWL file
- fixing labels 
- new terms: `mwo:openForUse (open for use)` and `mwo:ReferenceDataset`
- add `rdfs:seeAlso` to ontology metadata
- fixing license type

## Contributors to this release

|first name | last name | ORCID|
|--- | --- | ---|
|Abril | Azocar Guzman | https://orcid.org/0000-0001-7564-7990|
|Said | Fathalla | https://orcid.org/0000-0002-2818-5890|
|Angelika | Gedsun | https://orcid.org/0000-0002-1141-7704|
|Volker | Hofmann | https://orcid.org/0000-0002-5149-603X|
|Stefan | Sandfeld | https://orcid.org/0000-0001-9560-4728|


## License 

[Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/legalcode)

