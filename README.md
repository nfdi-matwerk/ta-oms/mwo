# The MatWerk Ontology (MWO)

The MatWerk ontology represents research data and related activities of the MSE community. A first version of the ontology will be simplified, focusing on: (i) community structure: researchers, research projects, universities, institutions; (ii) infrastructure: software, workflows, controlled vocabularies, instruments, facilities, educational resources and events; and (iii) data: repositories, databases, scientific publications, published datasets and reference data.								

**PID:** http://purls.helmholtz-metadaten.de/mwo

**Language:** [OWL](https://www.w3.org/OWL/)

**Website:** [https://nfdi-matwerk.de/](https://nfdi-matwerk.de/)

## Creators

|first name | last name | ORCID|
|--- | --- | ---|
|Abril | Azocar Guzman | https://orcid.org/0000-0001-7564-7990|
|Ahmad Zainul | Ihsan | https://orcid.org/0000-0002-1008-4530|
|Said | Fathalla | https://orcid.org/0000-0002-2818-5890|
|Angelika | Gedsun | https://orcid.org/0000-0002-1141-7704|


## Contributors

|first name | last name | ORCID|
|--- | --- | ---|
|Volker | Hofmann | https://orcid.org/0000-0002-5149-603X|
|Ebrahim | Norouzi | https://orcid.org/0000-0002-2691-6995|
|Amir | Laadhar | https://orcid.org/0000-0001-9106-8825|
|Felix | Fritzen | https://orcid.org/0000-0003-4926-0068|
|Harald | Sack | https://orcid.org/0000-0001-7069-9804|
|Stefan | Sandfeld | https://orcid.org/0000-0001-9560-4728|


## License 

[Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/legalcode)

## Acknowledgement

Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under the National
Research Data Infrastructure – NFDI 38/1 – project number 460247524.

<img src="images/Logo_NFDI-MatWerk.png" width="300">
